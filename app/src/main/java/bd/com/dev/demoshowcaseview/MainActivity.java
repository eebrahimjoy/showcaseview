package bd.com.dev.demoshowcaseview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.listener.GuideListener;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ShowIntro("SetTheme", "Select Theme and Apply on your video", R.id.btnOne, 1);

    }


    private void ShowIntro(String title, String text, int viewId, final int type) {

        new GuideView.Builder(this)
                .setTitle(title)
                .setContentText(text)
                .setTargetView(findViewById(viewId))
                .setContentTextSize(12)//optional
                .setTitleTextSize(14)//optional
                .setDismissType(DismissType.anywhere) //optional - default dismissible by TargetView
                .setGuideListener(new GuideListener() {
                    @Override
                    public void onDismiss(View view) {
                        if (type == 1) {
                            ShowIntro("Editor", "Edit any photo from selected photos than Apply on your video", R.id.btnTwo, 2);
                        } else if (type == 2) {
                            ShowIntro("Duration", "Set duration between photos", R.id.btnThree, 3);
                        } else if (type == 3) {
                            ShowIntro("Filter", "Add filter to video ", R.id.btnFour, 4);
                        } else if (type == 4) {
                            ShowIntro("Filter", "Add filter to video ", R.id.btnFive, 5);
                        } else if (type == 5) {

                            //Save a value in shared preference for manage the view.

                        }
                    }
                })
                .build()
                .show();
    }


}
